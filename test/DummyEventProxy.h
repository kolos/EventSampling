/*
 * DummyEventProxy.h
 *
 *  Created on: Dec 22, 2023
 *      Author: kolos
 */

#ifndef TEST_DUMMYEVENTPROXY_H_
#define TEST_DUMMYEVENTPROXY_H_

#include <memory>
#include <span>
#include <vector>

#include <eformat/Header.h>

class DummyEventProxy {
public:
    DummyEventProxy(uint32_t size, uint32_t l1id) :
        m_size(size),
        m_data(new uint32_t[m_size]),
        m_header_proxy((uint32_t*)&m_header)
    {
        m_header.m_event_size = m_size + m_header.m_header_size;
        m_header.m_l1id = l1id;
    }

    void serialize(std::vector<std::span<uint32_t>> & d) {
        d.push_back(std::span((uint32_t*)&m_header, m_header.m_header_size));
        d.push_back(std::span(m_data.get(), m_size));
    }

    const eformat::write::FullEventFragment & header() const {
        return m_header_proxy;
    }

    const std::vector<eformat::helper::StreamTag> & streamTags() const {
        return m_stream_tags;
    }

private:
    struct EventHeader {
        uint32_t m_header_marker = 0xaa1234aa;
        uint32_t m_event_size;
        uint32_t m_header_size = sizeof(EventHeader) / sizeof(uint32_t);
        uint32_t m_format_version = 0x05000000;
        uint32_t m_source = 0x007c0000; // HLT
        uint32_t m_status_words_number = 0;
        uint32_t m_checksum_type = 0; // no check-sum is present
        uint32_t m_lhc_info[4] = { (uint32_t)time(0), 0, 0, 0 };
        uint32_t m_run_type = 0x0000000f; // 'test'
        uint32_t m_run_number = 0;
        uint32_t m_lumi_block = 0;
        uint32_t m_l1id;
        uint32_t m_bcid;
        uint32_t m_l1_trigger_type;
        uint32_t m_compression_type = 0; // no compression
        uint32_t m_payload_size;
        uint32_t m_trigger_info[4] = { 0, 0, 0, 0 };
    };

private:
    uint32_t m_size;
    std::shared_ptr<uint32_t[]> m_data;
    EventHeader m_header;
    eformat::write::FullEventFragment m_header_proxy;
    std::vector<eformat::helper::StreamTag> m_stream_tags;
};

#endif /* TEST_DUMMYEVENTPROXY_H_ */
