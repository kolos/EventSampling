/*
 * monitor_main.cpp
 *
 *  Created on: Dec 20, 2023
 *      Author: kolos
 */
#include <chrono>
#include <csignal>

#include <boost/program_options.hpp>

#include <ipc/core.h>

#include <EventSampling/EventStream.h>

using namespace EventSampling;
using namespace boost::program_options;
using namespace std::literals::chrono_literals;

volatile std::sig_atomic_t signal_status = 0;

void signal_handler(int s) {
    signal_status = s;
}

namespace {
    std::list<std::pair<std::string, std::string>> set_default_options(
        int & ac, char ** & av) {
        std::list<std::pair<std::string, std::string> > options =
            IPCCore::extractOptions(ac, av);
        options.push_front(
            std::make_pair(std::string("clientCallTimeOutPeriod"), std::string("2000")));
        options.push_front(
            std::make_pair(std::string("threadPerConnectionPolicy"), std::string("0")));
        options.push_front(
            std::make_pair(std::string("threadPoolWatchConnection"), std::string("0")));
        options.push_front(
            std::make_pair(std::string("maxServerThreadPoolSize"), std::string("20")));

        return options;
    }
}

int main(int ac, char ** av) {
    setenv("TDAQ_ERS_VERBOSITY_LEVEL", "-1", 0);

    std::string sampler_type;
    std::vector<std::string> sampler_names;
    std::string stream_name = "test";
    std::string criteria = "{}";
    float processing_time = 0.1;
    IPCPartition partition;

    try {
        IPCCore::init(set_default_options(ac, av));

        options_description description("Options");
        description.add_options()("help,h", "produce help message")
          ("verbose,v", "Print a line for every received event")
          ("partition,p", value<std::string>(), "Partition name")
          ("sampler-type,t", value(&sampler_type)->required(), "Sampler type")
          ("sampler-names,n", value(&sampler_names)->multitoken(), "Sampler names")
          ("stream-name,s", value(&stream_name), "Monitoring stream name")
          ("criteria,c", value(&criteria), "Selection criteria")
          ("processing-time,w", value(&processing_time)->default_value(processing_time),
              "Time to simulate event processing (seconds)")
        ;

        variables_map arguments;
        store(parse_command_line(ac, av, description), arguments);
        if (arguments.count("help")) {
          std::cout << "Event monitor test application:" << std::endl;
          std::cout << description;
          return 0;
        }

        notify(arguments);

        if (arguments.count("partition")) {
            partition = IPCPartition(arguments["partition"].as<std::string>());
        }

        std::signal(SIGINT, signal_handler);
        std::signal(SIGTERM, signal_handler);

        std::unique_ptr<EventStream> stream;

        stream.reset(new EventStream(
                partition, sampler_type, stream_name, criteria, sampler_names));

        ERS_LOG("Connected to '" << sampler_type << "' sampler(s) using '"
            << criteria << "' criteria");

        uint32_t event_counter{};
        while (signal_status == 0) {
            std::optional<Event> e = stream->readEvent(1s);
            if (not e) {
                ERS_LOG("No events were received during last second");
                continue;
            }

            if (arguments.count("verbose")) {
                auto size = e->getSize();
                auto data = e->getData();
                if (size == 0) {
                    ERS_LOG("Data fragment #" << ++event_counter
                        << " of zero length has been received");
                }
                else if (size < 15) {
                    ERS_LOG("Data fragment #" << ++event_counter << " of size " << size
                        << " of type " << std::hex << data[0] << " has been received");
                } else {
                    if (data[0] == 0xaa1234aa) {
                        ERS_LOG("Full event #" << ++event_counter << " of size " << size
                            << " with l1id 0x" << std::hex << data[14] << " has been received");
                    } else if (data[0] == 0xdd1234dd) {
                        // We assume that ROB header has two status words
                        ERS_LOG("ROB fragment #" << ++event_counter << " of size " << size
                            << " with l1id 0x" << std::hex << data[14] << " has been received");
                    } else {
                        ERS_LOG("Data fragment #" << ++event_counter << " of size " << size
                            << " of unknown type " << std::hex << data[0] << " has been received");
                    }
                }
            }
            std::this_thread::sleep_for(processing_time * 1s);
        }
    } catch (ers::Issue & e) {
        ers::fatal(e);
        return 1;
    } catch (std::exception & e) {
        std::cerr << "Error: " << e.what() << "\n";
        return 1;
    }

    ERS_LOG("Event monitor received signal " << signal_status << " and is going to be terminated");

    return 0;
}
