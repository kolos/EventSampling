/*
 * sampler_main.cpp
 *
 *  Created on: Dec 20, 2023
 *      Author: kolos
 */
#include <chrono>
#include <csignal>

#include <boost/program_options.hpp>

#include <ipc/core.h>

#include <EventSampling/EventSampler.h>

#include "DummyEventProxy.h"

using namespace EventSampling;
using namespace boost::program_options;
using namespace std::literals::chrono_literals;

volatile std::sig_atomic_t signal_status = 0;

void signal_handler(int s) {
    signal_status = s;
}

namespace {
    std::list<std::pair<std::string, std::string>> set_default_options(
        int & ac, char ** & av) {
        std::list<std::pair<std::string, std::string> > options =
            IPCCore::extractOptions(ac, av);
        options.push_front(
            std::make_pair(std::string("clientCallTimeOutPeriod"), std::string("2000")));
        options.push_front(
            std::make_pair(std::string("threadPerConnectionPolicy"), std::string("0")));
        options.push_front(
            std::make_pair(std::string("threadPoolWatchConnection"), std::string("0")));
        options.push_front(
            std::make_pair(std::string("maxServerThreadPoolSize"), std::string("20")));

        return options;
    }
}

int main(int ac, char ** av) {

    std::string sampler_type;
    std::string sampler_name;
    IPCPartition partition;
    float l1_rate = 1.;
    uint32_t event_size = 1024;

    try {
        IPCCore::init(set_default_options(ac, av));

        options_description description("Options");
        description.add_options()("help,h", "produce help message")
          ("partition,p", value<std::string>(), "Partition name")
          ("sampler-type,t", value(&sampler_type)->required(), "Sampler type")
          ("sampler-name,n", value(&sampler_name)->required(), "Sampler name")
          ("l1-rate,r", value(&l1_rate)->default_value(l1_rate),
              "Event generation rate (Hz)")
          ("event-size,s", value(&event_size)->default_value(event_size),
              "Event size in 4-byte words")
        ;

        variables_map arguments;
        store(parse_command_line(ac, av, description), arguments);
        if (arguments.count("help")) {
          std::cout << "Event sampler test application:" << std::endl;
          std::cout << description;
          return 0;
        }

        notify(arguments);

        if (arguments.count("partition")) {
            partition = IPCPartition(arguments["partition"].as<std::string>());
        }

        if (l1_rate == 0.) {
            std::cerr << "Error: L1 rate must be greater than zero";
            return 1;
        }

        std::signal(SIGINT, signal_handler);
        std::signal(SIGTERM, signal_handler);

        EventSampler s(partition, sampler_type, sampler_name);

        ERS_LOG("Event sampler '" << sampler_type << ":" << sampler_name
            << "' has been started in '" << partition.name() << "' partition");

        uint32_t i = 0;
        while (signal_status == 0) {
            s.pushEvent(::pro::make_proxy<CopyableEvent>(
                ::DummyEventProxy(event_size, i++)));

            std::this_thread::sleep_for(1. / l1_rate * 1s);
        }
    } catch (ers::Issue & e) {
        ers::fatal(e);
        return 1;
    } catch (std::exception & e) {
        std::cerr << "Error: " << e.what() << "\n";
        return 1;
    }

    ERS_LOG("Event sampler '" << sampler_type << ":" << sampler_name
        << "' received signal " << signal_status << " and has been terminated");

    return 0;
}
