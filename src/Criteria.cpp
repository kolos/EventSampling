/*
 * Criteria.cpp
 *
 *  Created on: Dec 21, 2023
 *      Author: kolos
 */

#include <EventSampling/detail/Criteria.h>

#include <nlohmann/json.hpp>

using namespace nlohmann;
using namespace EventSampling::detail;

namespace {
    const std::string valid_keys("l1_type, l1_bits, status, streams");
}

Criteria::Criteria(const std::string & criteria)
try : m_string(criteria.empty() ? std::string("{}") : criteria)
{
    json j = json::parse(m_string);
    for (auto& [key, value] : j.items()) {
        if (key == "l1_type") {
            if (not value.is_number()) {
                throw InvalidCriteriaValueException(ERS_HERE, criteria, "", key, value.dump());
            }
            m_l1_type = value.get<uint32_t>();
        }
        else if (key == "l1_bits") {
            if (not value.is_array()) {
                throw InvalidCriteriaValueException(ERS_HERE, criteria, "", key, value.dump());
            }
            for (auto & v : value) {
                m_l1_bits.push_back(v.get<uint16_t>());
            }
        }
        else if (key == "status") {
            if (not value.is_array()) {
                throw InvalidCriteriaValueException(ERS_HERE, criteria, "", key, value.dump());
            }
            for (auto & v : value) {
                m_status.push_back(v.get<uint32_t>());
            }
        }
        else if (key == "streams") {
            if (not value.is_array()) {
                throw InvalidCriteriaValueException(ERS_HERE, criteria, "", key, value.dump());
            }
            for (auto & stream : value) {
                if (not stream.begin().value().is_array()) {
                    throw InvalidCriteriaValueException(ERS_HERE, criteria, "", stream.begin().key(),
                        stream.begin().value().dump());
                }
                for (auto& n : stream.begin().value()) {
                    m_streams[stream.begin().key()].insert(n.dump());
                }
            }
        } else {
            throw InvalidCriteriaKeyException(ERS_HERE, criteria,
                "- valid keys are '" + valid_keys + "'", key);
        }
    }
} catch (ers::Issue & ) {
    throw;
} catch (std::exception & e) {
    throw InvalidCriteriaException(ERS_HERE, criteria, "parsing failed", e);
}
