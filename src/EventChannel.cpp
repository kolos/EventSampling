/*
 * EventChannel.cpp
 *
 *  Created on: Dec 21, 2023
 *      Author: kolos
 */

#include <eformat/StreamTag.h>

#include <EventSampling/detail/EventChannel.h>

using namespace EventSampling::detail;

EventChannel::EventChannel(const std::string & criteria)
try :
    Base(criteria),
    m_criteria(criteria)
{}
catch (std::exception & e)
{
    throw InvalidCriteria(e.what());
}

void EventChannel::eventReady(CopyableEventProxy e) {

    const eformat::write::FullEventFragment & h = e.invoke<header>();
    bool matched = h.lvl1_trigger_type() ==
        m_criteria.l1Type().value_or(h.lvl1_trigger_type());

    if (not matched) {
        return ;
    }

    for (auto bit : m_criteria.l1Bits()) {
        matched = false;
        uint32_t w = bit>>4;
        if (w < h.nlvl1_trigger_info() and (h.lvl1_trigger_info()[w] & (1<<(bit & 0x1f)))) {
            matched = true;
            break;
        }
    }

    if (not m_criteria.streams().empty()) {
        matched = false;
        Criteria::Streams streams = m_criteria.streams();
        for (auto & t : e.invoke<streamTags>()) {
            auto it = streams.find(t.type);
            if (it != streams.end() and it->second.contains(t.name)) {
                matched = true;
                break;
            }
        }
    }

    if (matched) {
        Base::eventReady(e);
    }
}
