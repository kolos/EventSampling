/*
 * Event.cpp
 *
 *  Created on: Dec 22, 2023
 *      Author: kolos
 */
#include <iostream>

#include <EventSampling/Event.h>
#include <EventSampling/detail/EventReceiver.h>

using namespace EventSampling;
using namespace std::chrono;

Event::Event(const EventSampling::EventBuffer & b) :
    m_time(steady_clock::now())
{
    for (uint32_t i = 0; i < b.length(); ++i) {
        m_size += b[i].length();
    }
    m_data.reset(new uint32_t[m_size]);

    uint32_t * p = m_data.get();
    for (uint32_t i = 0; i < b.length(); ++i) {
        memcpy(p, b[i].get_buffer(), sizeof(uint32_t) * b[i].length());
        p += b[i].length();
    }
}

void Event::processingStarted(const std::shared_ptr<detail::EventReceiver> & r) {
    m_stop_watch = std::make_shared<Event::StopWatch>(r);
}

Event::StopWatch::StopWatch(std::shared_ptr<detail::EventReceiver> r) :
    m_time(steady_clock::now()),
    m_receiver(r)
{}

Event::StopWatch::~StopWatch() {
    if(auto tmp = m_receiver.lock()) {
        tmp->lastEventProcessingTime(
            duration_cast<microseconds>(steady_clock::now() - m_time));
    }
}
