/*
 * ReceiverGroup.cpp
 *
 *  Created on: Dec 20, 2023
 *      Author: kolos
 */

#include <EventSampling/detail/ReceiverGroup.h>

#include <ers/ers.h>

using namespace EventSampling::detail;

void ReceiverGroup::eventReady(CopyableEventProxy e) {
    auto now = std::chrono::steady_clock::now();
    ERS_DEBUG(1, "Event is ready to be sent to '" << m_stream_name << " group");

    std::unique_lock l(m_mutex);
    auto receivers(m_receivers);
    l.unlock();

    for (auto it = receivers.begin(); it != receivers.end(); ++it) {
        if (now < it->first) {
            ERS_DEBUG(1, "There are no receivers in '"
                << m_stream_name << "' group ready to get events");
            ++m_rejected_events;
            return ;
        }

        auto r = it->second;
        try {
            int64_t timeout = r->timeout_us();
            ERS_DEBUG(2, "Got " << timeout << "us timeout from " << r << " receiver");
            if (timeout == 0) {
                std::vector<std::span<uint32_t>> data;
                e.invoke<serialize>(data);

                EventSampling::EventBuffer buf(data.size());
                buf.length(data.size());
                uint32_t i = 0;
                for (auto & chunk : data) {
                    buf[i].replace(chunk.size(), chunk.size(), chunk.data(), 0);
                    ++i;
                }

                ERS_DEBUG(2, "Sending event to " << r << " receiver of '"
                          << m_stream_name << "' group");
                r->receive(buf);
                ERS_DEBUG(2, "Event has been sent to " << r << " receiver of '"
                          << m_stream_name << "' group");
                ++m_sent_events;
                return ;
            }

            auto time_point = now + std::chrono::microseconds(timeout);

            l.lock();
            auto valid = std::erase_if(m_receivers, [r](auto & v) {
                return v.second->_is_equivalent(r);
            });
            if (valid) {
                m_receivers.insert(std::make_pair(time_point, r));
            }
            l.unlock();
        }
        catch (...) {
            remove(r);
        }
    }

    ERS_DEBUG(1, "There are no receivers in '"
        << m_stream_name << "' group ready to get events");

    ++m_rejected_events;
}
