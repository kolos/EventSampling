/*
 * EventSampler.cpp
 *
 *  Created on: Dec 20, 2023
 *      Author: kolos
 */

#include <ers/ers.h>
#include <EventSampling/detail/EventSource.h>

#include <EventSampling/EventSampler.h>

using namespace EventSampling;

EventSampler::EventSampler(const IPCPartition & p, const std::string & sampler_type,
             const std::string & sampler_name) :
    m_event_source(std::shared_ptr<detail::EventSource>(
        new detail::EventSource(p, sampler_type + ":" + sampler_name),
        [](auto * p){p->_destroy();}))
{
    m_event_source->publish();
}

EventSampler::~EventSampler() {
    try {
        m_event_source->withdraw();
    } catch (...) {

    }
}

void EventSampler::pushEvent(CopyableEventProxy e) {
    m_event_source->pushEvent(e);
}
