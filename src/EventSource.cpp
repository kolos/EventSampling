/*
 * EventSource.cpp
 *
 *  Created on: Dec 21, 2023
 *      Author: kolos
 */

#include <EventSampling/detail/EventSource.h>

#include <ers/ers.h>

using namespace EventSampling::detail;

void EventSource::connect(const char * criteria, const char * stream_name,
             EventSampling::Receiver_ptr r) {
    ERS_DEBUG(1, "connect request received, criteria = " << criteria
        << " stream_name = " << stream_name);
    add(criteria, stream_name, r);
    ERS_DEBUG(1, "done");
}

void EventSource::disconnect(const char * criteria, const char * stream_name,
                EventSampling::Receiver_ptr r) {
    ERS_DEBUG(1, "disconnect request received, criteria = " << criteria
        << " stream_name = " << stream_name);
    remove(criteria, stream_name, r);
    ERS_DEBUG(1, "done");
}
