/*
 * EventStream.cpp
 *
 *  Created on: Dec 20, 2023
 *      Author: kolos
 */
#include <format>
#include <boost/asio/ip/host_name.hpp>

#include <EventSampling/EventStream.h>
#include <EventSampling/detail/Criteria.h>
#include <EventSampling/detail/EventReceiver.h>

#include <ers/ers.h>
#include <ipc/exceptions.h>

using namespace EventSampling;

namespace {
    std::string unique_process_id() {
        const char *name = getenv("TDAQ_APPLICATION_NAME");

        if (name) {
            return std::string(name);
        }
        return std::format("{}:{}", boost::asio::ip::host_name(), getpid());
    }
}

EventStream::EventStream(const IPCPartition & partition,
    const std::string & sampler_type,
    const std::string & stream_name,
    const std::string & criteria,
    const std::vector<std::string> & sampler_names,
    uint32_t minimal_buffer_size) :
        m_partition(partition),
        m_stream_name(stream_name),
        m_criteria(detail::Criteria(criteria).toString()),
        m_event_receiver(
            new detail::EventReceiver(m_partition, unique_process_id(), minimal_buffer_size),
            [](detail::EventReceiver * e){e->_destroy();}
        )
{
    if (sampler_names.empty()) {
        std::map<std::string, EventSampling::Source_var> objects;
        m_partition.getObjects<EventSampling::Source>(objects);

        for (auto pair : objects) {
            if (pair.first.find(sampler_type) != 0) {
                continue;
            }
            try {
                pair.second->connect(m_criteria.c_str(), m_stream_name.c_str(),
                    m_event_receiver->_this());
                m_names.push_back(pair.first);
            }
            catch (InvalidCriteria & e) {
                // must not happen because we checked criteria before sending it
                throw InvalidCriteriaException(ERS_HERE, m_criteria, std::string(e.what));
            }
            catch (CORBA::SystemException & e) {
                ers::warning(daq::ipc::CorbaSystemException(ERS_HERE, &e));
            }
            catch (daq::ipc::Exception & e) {
                ers::warning(CannotConnectException(ERS_HERE, sampler_type, pair.first, e));
            }
        }
        if (m_names.empty()) {
            throw CannotConnectException(ERS_HERE, sampler_type, "any");
        }
        return;
    }

    for (auto & n : sampler_names) {
        auto name = sampler_type + ":" + n;
        try {
            EventSampling::Source_ptr s = m_partition.lookup<EventSampling::Source>(name);
            s->connect(m_criteria.c_str(), m_stream_name.c_str(), m_event_receiver->_this());
            m_names.push_back(name);
        }
        catch (InvalidCriteria & e) {
            // must not happen because we checked criteria before sending it
            throw InvalidCriteriaException(ERS_HERE, m_criteria, std::string(e.what));
        }
        catch (daq::ipc::Exception & e) {
            throw CannotConnectException(ERS_HERE, sampler_type, name, e);
        }
        catch (CORBA::SystemException & e) {
            throw CannotConnectException(ERS_HERE, sampler_type, name,
                daq::ipc::CorbaSystemException(ERS_HERE, &e));
        }
    }
}

EventStream::~EventStream() {
    for (auto & n : m_names) {
        try {
            EventSampling::Source_ptr s = m_partition.lookup<EventSampling::Source>(n);
            s->disconnect(m_criteria.c_str(), m_stream_name.c_str(), m_event_receiver->_this());
        }
        catch (CORBA::SystemException & e) {
            ers::log(daq::ipc::CorbaSystemException(ERS_HERE, &e));
        }
    }
}

std::optional<Event> EventStream::readEvent(std::chrono::milliseconds timeout) {
    return m_event_receiver->getEvent(std::chrono::steady_clock::now() + timeout);
}
