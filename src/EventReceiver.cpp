/*
 * EventReceiver.cpp
 *
 *  Created on: Dec 20, 2023
 *      Author: kolos
 */

#include <EventSampling/detail/EventReceiver.h>

#include <ers/ers.h>

using namespace EventSampling;
using namespace EventSampling::detail;
using namespace std::chrono;

EventReceiver::EventReceiver(const IPCPartition & p, const std::string & name,
                             uint32_t minimal_buffer_size) :
    m_queue_size(minimal_buffer_size),
    m_info(p, "Monitoring." + name),
    m_alarm(1, [this]{return publishInfo();})
{ }

EventReceiver::~EventReceiver() {
    m_alarm.suspend();
    try {
        m_info.remove();
    } catch (ers::Issue& e) {
        ers::debug(e, 1);
    }
    m_condition.notify_one();
}

std::optional<Event> EventReceiver::getEvent(
    const time_point<steady_clock> & timeout) {

    ERS_DEBUG(1, "Event requested");

    std::unique_lock l(m_mutex);
    bool queue_is_empty = m_event_queue.empty();
    while (queue_is_empty) {
        if (m_queue_was_full) {
            m_queue_size *= 2;
            m_queue_was_full = false;
        }
        if (std::cv_status::timeout == m_condition.wait_until(l, timeout)) {
            ERS_DEBUG(1, "Event buffer is empty");
            return {};
        }
    }

    if (not m_event_queue.empty()) {
        Event e(m_event_queue.front());
        e.processingStarted(shared_from_this());
        m_event_queue.pop();
        ERS_DEBUG(2, "Event read, buffer now contains "
                  << m_event_queue.size() << " events");
        return e;
    }

    ERS_DEBUG(1, "Event buffer is empty");
    return {};
}

void EventReceiver::lastEventProcessingTime(microseconds us) {
    std::unique_lock l(m_mutex);
    m_mean_processing_time = (m_mean_processing_time * m_processed_events + us)
        /(m_processed_events + 1);
    m_processed_events++;
}

void EventReceiver::receive(const EventSampling::EventBuffer & b) {
    ERS_DEBUG(1, "Event received");

    std::unique_lock l(m_mutex);
    m_received_events++;

    if (m_expected_events) {
        --m_expected_events;
    } else {
        m_dropped_events++;
        return ;
    }

    m_event_queue.push(Event(b));
    m_condition.notify_one();

    ERS_DEBUG(1, "Event buffer contains " << m_event_queue.size() << " events");
}

CORBA::Long EventReceiver::timeout_us() {
    ERS_DEBUG(2, "timeout_us request received");

    std::unique_lock l(m_mutex);

    auto us = m_mean_processing_time;
    if (us.count() == 0 and not m_event_queue.empty()) {
        us = duration_cast<microseconds>(
            steady_clock::now() - m_event_queue.front().getReceptionTime());
    }

    if ((m_event_queue.size() + m_expected_events) >= m_queue_size) {
        m_queue_was_full = not m_expected_events;
        return us.count() * (m_event_queue.size() + 1);
    }

    ++m_expected_events;
    ERS_DEBUG(2, "Ready to receive events: "
        << m_expected_events << " events expected, "
        << m_event_queue.size() << " events already in the buffer");
    return 0;
}

bool EventReceiver::publishInfo() {
    {
        std::unique_lock l(m_mutex);
        m_info.m_buffer_occupancy = m_event_queue.size();
        m_info.m_buffer_size = m_queue_size;
        m_info.m_received_events = m_received_events;
        m_info.m_dropped_events = m_dropped_events;
        m_info.m_buffered_events = m_processed_events;
    }
    try {
        m_info.checkin();
    } catch (ers::Issue & e) {
        ers::debug(e, 1);
    }
    return true;
}
