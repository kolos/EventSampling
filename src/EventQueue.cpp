/*
 * EventQueue.cpp
 *
 *  Created on: Dec 20, 2023
 *      Author: kolos
 */
#include <chrono>

#include <EventSampling/detail/EventQueue.h>

using namespace EventSampling::detail;
using namespace std::chrono_literals;

void EventQueue::run() {
    while (m_active) {
        CopyableEventProxy e;
        if (not m_event_queue.try_pop(e)) {
            std::this_thread::sleep_for(10us);
            continue;
        }

        eventReady(e);
    }
}
