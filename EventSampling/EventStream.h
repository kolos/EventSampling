/*
 * EventStream.h
 *
 *  Created on: Dec 20, 2023
 *      Author: kolos
 */

#ifndef EVENTSAMPLING_EVENTSTREAM_H_
#define EVENTSAMPLING_EVENTSTREAM_H_

#include <chrono>
#include <functional>
#include <string>
#include <vector>

#include <ers/ers.h>
#include <ipc/partition.h>

#include "Event.h"

ERS_DECLARE_ISSUE(EventSampling, CannotConnectException,
                  "Cannot connect to " << sampler_name << " event sampler of '"
                  << sampler_type << "' type",
                  ((std::string)sampler_type) ((std::string)sampler_name))

namespace EventSampling {
    namespace detail {
        class EventReceiver;
    }

    class EventStream {
    public:
        EventStream(const IPCPartition & partition,
                    const std::string & sampler_type,
                    const std::string & stream_name,
                    const std::string & criteria = "",
                    const std::vector<std::string> & sampler_names = {},
                    uint32_t minimal_buffer_size = 10);

        ~EventStream();

        std::optional<Event> readEvent(std::chrono::milliseconds timeout);

        const std::vector<std::string> & getConnectedSamplers() const {
            return m_names;
        }

    private:
        typedef std::shared_ptr<detail::EventReceiver> Receiver;

        const IPCPartition m_partition;
        const std::string m_stream_name;
        const std::string m_criteria;
        std::vector<std::string> m_names;
        Receiver m_event_receiver;
    };
}

#endif /* EVENTSAMPLING_EVENTSTREAM_H_ */
