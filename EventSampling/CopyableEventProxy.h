/*
 * CopyableEventProxy.h
 *
 *  Created on: Dec 21, 2023
 *      Author: kolos
 */

#ifndef EVENTSAMPLING_COPYABLEEVENTPROXY_H_
#define EVENTSAMPLING_COPYABLEEVENTPROXY_H_

#include <span>
#include <vector>

#include <eformat/write/FullEventFragment.h>
#include <eformat/StreamTag.h>

#include <EventSampling/detail/microsoft/proxy.h>

namespace EventSampling {
    PRO_DEF_MEMBER_DISPATCH(header, const eformat::write::FullEventFragment&());
    PRO_DEF_MEMBER_DISPATCH(streamTags, const std::vector<eformat::helper::StreamTag>&());
    PRO_DEF_MEMBER_DISPATCH(serialize, void(std::vector<std::span<uint32_t>> &));
    PRO_DEF_FACADE(CopyableEvent, std::tuple<header, streamTags, serialize>,
                   ::pro::copyable_pointer_constraints);

    typedef ::pro::proxy<CopyableEvent> CopyableEventProxy;
}

#endif /* EVENTSAMPLING_COPYABLEEVENTPROXY_H_ */
