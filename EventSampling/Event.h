/*
 * Event.h
 *
 *  Created on: Dec 20, 2023
 *      Author: kolos
 */

#ifndef EVENTSAMPLING_EVENT_H_
#define EVENTSAMPLING_EVENT_H_

#include <chrono>
#include <memory>

#include <EventSampling/EventSampling.hh>

namespace EventSampling {
    namespace detail {
        class EventReceiver;
    }

    class Event {
        friend class detail::EventReceiver;

    public:
        uint32_t * getData() const {
            return m_data.get();
        }

        uint32_t getSize() const {
            return m_size;
        }

        const std::chrono::steady_clock::time_point getReceptionTime() const {
            return m_time;
        }

    private:
        Event(const EventSampling::EventBuffer & b);

        void processingStarted(const std::shared_ptr<detail::EventReceiver> & r);

    private:
        // Used to measure event processing time
        struct StopWatch {
            StopWatch(std::shared_ptr<detail::EventReceiver> r);
            ~StopWatch();

        private:
            std::chrono::steady_clock::time_point m_time;
            std::weak_ptr<detail::EventReceiver> m_receiver;
        };

        std::chrono::steady_clock::time_point m_time;
        std::shared_ptr<StopWatch> m_stop_watch;
        uint32_t m_size{};
        std::shared_ptr<uint32_t[]> m_data;
    };
}

#endif /* EVENTSAMPLING_EVENT_H_ */
