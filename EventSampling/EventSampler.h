/*
 * EventSampler.h
 *
 *  Created on: Dec 20, 2023
 *      Author: kolos
 */

#ifndef EVENTSAMPLING_EVENTSAMPLER_H_

#include <ipc/partition.h>

#include "CopyableEventProxy.h"

namespace EventSampling {
    namespace detail {
        class EventSource;
    }

    class EventSampler {
    public:
        EventSampler(const IPCPartition & p, const std::string & sampler_type,
                     const std::string & sampler_name);

        ~EventSampler();

        void pushEvent(CopyableEventProxy e);

    private:
        std::shared_ptr<detail::EventSource> m_event_source;
    };
}

#endif
