/*
 * EventChannel.h
 *
 *  Created on: Dec 20, 2023
 *      Author: kolos
 */

#ifndef EVENTSAMPLING_EVENTCHANNEL_H_
#define EVENTSAMPLING_EVENTCHANNEL_H_

#include <mutex>
#include <thread>

#include <ipc/namedobject.h>

#include <EventSampling/EventSampling.hh>

#include "Criteria.h"
#include "EventQueue.h"
#include "RecursiveMap.h"
#include "ReceiverGroup.h"

namespace EventSampling {
namespace detail {
    class EventChannel : public RecursiveMap<std::string, ReceiverGroup>
    {
        typedef RecursiveMap<std::string, ReceiverGroup> Base;

    public:
        explicit EventChannel(const std::string & criteria);

        void eventReady(CopyableEventProxy e) override;

    private:
        Criteria m_criteria;
    };
}}

#endif /* EVENTSAMPLING_EVENTCHANNEL_H_ */
