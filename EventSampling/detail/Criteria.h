/*
 * Criteria.h
 *
 *  Created on: Dec 21, 2023
 *      Author: kolos
 */

#ifndef EVENTSAMPLING_DETAIL_CRITERIA_H_
#define EVENTSAMPLING_DETAIL_CRITERIA_H_

#include <map>
#include <optional>
#include <set>
#include <string>
#include <vector>

#include <ers/ers.h>

ERS_DECLARE_ISSUE(EventSampling, InvalidCriteriaException,
                  "'" << criteria << "' event selection criteria " << reason,
                  ((std::string)criteria) ((std::string)reason))

ERS_DECLARE_ISSUE_BASE(EventSampling, InvalidCriteriaKeyException, InvalidCriteriaException,
                  "Invalid key '" << key << "' is used in ",
                  ((std::string)criteria) ((std::string)reason),
                  ((std::string)key))

ERS_DECLARE_ISSUE_BASE(EventSampling, InvalidCriteriaValueException, InvalidCriteriaException,
                  "Invalid value '" << value << "' is given for '" << key << "' key of ",
                  ((std::string)criteria) ((std::string)reason),
                  ((std::string)key) ((std::string)value))

namespace EventSampling {
namespace detail {
    class Criteria {
    public:
        typedef std::map<std::string, std::set<std::string>> Streams;

        explicit Criteria(const std::string & s);

        const std::string & toString() const {
            return m_string;
        }

        const Streams streams() const {
            return m_streams;
        }

        const std::vector<uint16_t> l1Bits() const {
            return m_l1_bits;
        }

        const std::vector<uint32_t> status() const {
            return m_status;
        }

        std::optional<uint32_t> l1Type() const {
            return m_l1_type;
        }

    private:
        std::string m_string;
        Streams m_streams;
        std::vector<uint16_t> m_l1_bits;
        std::vector<uint32_t> m_status;
        std::optional<uint32_t> m_l1_type;
    };
}}

#endif /* EVENTSAMPLING_DETAIL_CRITERIA_H_ */
