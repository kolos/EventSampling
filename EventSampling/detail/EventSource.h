/*
 * EventSource.h
 *
 *  Created on: Dec 20, 2023
 *      Author: kolos
 */

#ifndef EVENTSAMPLING_EVENTSOURCE_H_
#define EVENTSAMPLING_EVENTSOURCE_H_

#include <mutex>
#include <thread>

#include <ipc/alarm.h>
#include <ipc/namedobject.h>

#include <emon/SamplerInfoNamed.h>

#include <EventSampling/EventSampling.hh>
#include "EventChannel.h"
#include "RecursiveMap.h"

namespace EventSampling {
namespace detail {
    class EventSource : public IPCNamedObject<POA_EventSampling::Source>,
                        public RecursiveMap<std::string, EventChannel>
    {
    public:
        EventSource(const IPCPartition & p, const std::string & name) :
            IPCNamedObject<POA_EventSampling::Source>(p, name),
            RecursiveMap<std::string, EventChannel>(name),
            m_info(p, "Monitoring." + name),
            m_alarm(1, [this]{return publishInfo();}) {
        }

        ~EventSource() {
            m_alarm.suspend();
            try {
                m_info.remove();
            } catch (...) {}
        }

        void connect(const char * criteria, const char * stream_name,
                     EventSampling::Receiver_ptr r) override;

        void disconnect(const char * criteria, const char * stream_name,
                        EventSampling::Receiver_ptr r) override;

    private:
        bool publishInfo() {
            try {
                m_info.m_connected_monitors = descendantsNumber();
                m_info.m_active_channels = childrenNumber();
                m_info.m_events_sent = sentEventsNumber();
                m_info.checkin();
            } catch (...) {}
            return true;
        }

    private:
        emon::SamplerInfoNamed m_info;
        IPCAlarm m_alarm;
    };
}}

#endif /* EVENTSAMPLING_EVENTSOURCE_H_ */
