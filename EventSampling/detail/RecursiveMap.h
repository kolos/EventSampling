/*
 * RecursiveMap.h
 *
 *  Created on: Dec 20, 2023
 *      Author: kolos
 */

#ifndef EVENTSAMPLING_DETAIL_RECURSIVEMAP_H_
#define EVENTSAMPLING_DETAIL_RECURSIVEMAP_H_

#include <atomic>
#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <vector>

#include <EventSampling/CopyableEventProxy.h>
#include "EventQueue.h"

namespace EventSampling {
namespace detail {

    template <class K, class V>
    class RecursiveMap : public EventQueue {
    public:
        explicit RecursiveMap(const std::string & ) {}

        virtual ~RecursiveMap() {
            EventQueue::close();
        }

        template<class... Args>
        void add(const K & k, Args... args) {
            std::unique_lock l(m_mutex);

            auto it = m_children.find(k);
            if (it == m_children.end()) {
                ERS_DEBUG(1, "Adding new '" << k << "' entry to the map");
                it = m_children.insert(std::make_pair(k, std::make_shared<V>(k))).first;
                m_state_modified = true;
            }
            it->second->add(args...);
        }

        template<class... Args>
        bool remove(const K & k, Args... args) {
            std::unique_lock l(m_mutex);

            auto it = m_children.find(k);
            if (it != m_children.end()) {
                if (it->second->remove(args...)) {
                    ERS_DEBUG(1, "Removed '" << k << "' entry from the map");
                    m_children.erase(it);
                    m_state_modified = true;
                }
            }

            return m_children.empty();
        }

        size_t childrenNumber() {
            std::unique_lock l(m_mutex);
            return m_children.size();
        }

        size_t descendantsNumber() {
            std::unique_lock l(m_mutex);
            size_t n = 0;
            for (auto & c : m_children) {
                n += c.second->descendantsNumber();
            }
            return n;
        }

        size_t sentEventsNumber() {
            std::unique_lock l(m_mutex);
            size_t n = 0;
            for (auto & c : m_children) {
                n += c.second->sentEventsNumber();
            }
            return n;
        }

        void eventReady(CopyableEventProxy e) override {
            if (m_state_modified) {
                std::unique_lock l(m_mutex);
                m_children_cache = m_children;
                m_state_modified = false;
                ERS_DEBUG(1, "Children cache has been updated");
            }

            for (auto & s : m_children_cache) {
                s.second->pushEvent(e);
            }
        }

    protected:
        typedef std::map<K, std::shared_ptr<V>> Children;

        std::mutex m_mutex;
        std::atomic<uint32_t> m_state_modified{};
        Children m_children;
        Children m_children_cache;
    };
}}

#endif /* EVENTSAMPLING_DETAIL_RECURSIVEMAP_H_ */
