/*
 * ReceiverGroup.h
 *
 *  Created on: Dec 20, 2023
 *      Author: kolos
 */

#ifndef EVENTSAMPLING_DETAIL_RECEIVERGROUP_H_
#define EVENTSAMPLING_DETAIL_RECEIVERGROUP_H_

#include <algorithm>
#include <chrono>
#include <map>
#include <mutex>
#include <random>
#include <vector>

#include <EventSampling/EventSampling.hh>

#include <EventSampling/CopyableEventProxy.h>
#include "EventQueue.h"

namespace EventSampling {
namespace detail {

    class ReceiverGroup : public EventQueue {
    public:
        explicit ReceiverGroup(const std::string & stream_name) :
            m_stream_name(stream_name),
            m_seed(std::random_device{}()),
            m_rand(1, 1000)
        {}

        ~ReceiverGroup() {
            EventQueue::close();
        }

        size_t descendantsNumber() {
            std::unique_lock l(m_mutex);
            return m_receivers.size();
        }

        size_t sentEventsNumber() {
            return m_sent_events;
        }

        void add(EventSampling::Receiver_ptr r) {
            std::unique_lock l(m_mutex);
            m_receivers.insert(std::make_pair(
                std::chrono::steady_clock::time_point(std::chrono::seconds(m_rand(m_seed))),
                EventSampling::Receiver::_duplicate(r)));
            ERS_DEBUG(1, "A new receiver has been added,"
                      " total number of active receivers is " << m_receivers.size());
        }

        bool remove(EventSampling::Receiver_ptr r) {
            std::unique_lock l(m_mutex);
            auto num = std::erase_if(m_receivers, [r](auto & v) {
                return v.second->_is_equivalent(r);
            });
            ERS_DEBUG(1, num << " receiver(s) have been removed,"
                      " total number of active receivers is " << m_receivers.size());
            return m_receivers.empty();
        }

        void eventReady(CopyableEventProxy e) override;

    private:
        typedef std::multimap<
            std::chrono::steady_clock::time_point,
            EventSampling::Receiver_var> Receivers;

        const std::string m_stream_name;
        std::mt19937 m_seed;
        std::uniform_int_distribution<uint32_t> m_rand;
        std::atomic<uint64_t> m_sent_events{};
        std::atomic<uint64_t> m_rejected_events{};
        std::mutex m_mutex;
        Receivers m_receivers;
    };
}}

#endif /* EVENTSAMPLING_DETAIL_RECEIVERGROUP_H_ */
