/*
 * EventReceiver.h
 *
 *  Created on: Dec 20, 2023
 *      Author: kolos
 */

#ifndef EVENTSAMPLING_EVENTRECEIVER_H_
#define EVENTSAMPLING_EVENTRECEIVER_H_

#include <chrono>
#include <condition_variable>
#include <map>
#include <memory>
#include <mutex>
#include <optional>
#include <queue>

#include <ipc/alarm.h>
#include <ipc/object.h>
#include <ipc/partition.h>

#include <emon/MonitorInfoNamed.h>
#include <EventSampling/EventSampling.hh>

#include <EventSampling/Event.h>

namespace EventSampling {
namespace detail {
    class EventReceiver : public IPCObject<POA_EventSampling::Receiver>,
                          public std::enable_shared_from_this<EventReceiver>
    {
    public:
        EventReceiver(const IPCPartition & p, const std::string & name,
                      uint32_t minimal_buffer_size);

        ~EventReceiver();

        std::optional<Event> getEvent(
            const std::chrono::time_point<std::chrono::steady_clock> & wait_until);

        void lastEventProcessingTime(std::chrono::microseconds us);

        void receive(const EventSampling::EventBuffer & e) override;

        CORBA::Long timeout_us() override;

    private:
        bool publishInfo();

    private:
        typedef std::queue<EventSampling::Event> EventQueue;

        std::mutex m_mutex;
        std::condition_variable m_condition;
        std::chrono::microseconds m_mean_processing_time;
        bool m_queue_was_full = false;
        uint32_t m_queue_size;
        uint32_t m_expected_events{};
        uint32_t m_processed_events{};
        uint32_t m_received_events{};
        uint32_t m_dropped_events{};
        EventQueue m_event_queue;
        emon::MonitorInfoNamed m_info;
        IPCAlarm m_alarm;
    };
}}

#endif /* EVENTSAMPLING_EVENTRECEIVER_H_ */
