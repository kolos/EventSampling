/*
 * EventQueue.h
 *
 *  Created on: Dec 20, 2023
 *      Author: kolos
 */

#ifndef EVENTSAMPLING_DETAIL_EVENTQUEUE_H_
#define EVENTSAMPLING_DETAIL_EVENTQUEUE_H_

#include <thread>

#include <tbb/concurrent_queue.h>

#include <EventSampling/CopyableEventProxy.h>

namespace EventSampling {
namespace detail {

    class EventQueue {
    public:
        EventQueue() :
            m_thread([this]{run();})
        {
            m_event_queue.set_capacity(1000);
        }

        virtual ~EventQueue() {
            close();
        }

        void pushEvent(CopyableEventProxy e) {
            m_event_queue.try_push(e);
        }

        virtual void eventReady(CopyableEventProxy e) = 0;

        void close() {
            if (m_active) {
                m_active = false;
                m_thread.join();
            }
        }

    private:
        void run();

        std::atomic<bool> m_active = true;
        tbb::concurrent_bounded_queue<CopyableEventProxy> m_event_queue;
        std::thread m_thread;
    };
}}

#endif /* EVENTSAMPLING_DETAIL_EVENTQUEUE_H_ */
